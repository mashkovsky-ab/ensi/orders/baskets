<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id')->unique();

            $table->timestamps(6);
        });
        Schema::create('basket_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('basket_id');
            $table->unsignedBigInteger('offer_id');
            $table->unsignedBigInteger('seller_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedDecimal('qty', 18, 4);

            $table->timestamps(6);

            $table->foreign('basket_id')->on('baskets')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basket_items');
        Schema::dropIfExists('baskets');
    }
};
