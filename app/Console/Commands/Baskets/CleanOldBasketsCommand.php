<?php

namespace App\Console\Commands\Baskets;

use App\Domain\Baskets\Actions\Basket\CleanOldBasketsAction;
use Illuminate\Console\Command;

class CleanOldBasketsCommand extends Command
{
    protected $signature = 'baskets:clean-old';
    protected $description = 'Удалить старые корзины не привязанные к пользователям';

    public function handle(CleanOldBasketsAction $action)
    {
        $action->execute();
    }
}
