<?php

namespace App\Console;

use App\Console\Commands\Baskets\CleanOldBasketsCommand;
use Ensi\LaravelInitialEventPropagation\SetInitialEventArtisanMiddleware;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    public function bootstrap()
    {
        parent::bootstrap();
        (new SetInitialEventArtisanMiddleware())->handle();
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(CleanOldBasketsCommand::class)->hourlyAt(5);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
    }
}
