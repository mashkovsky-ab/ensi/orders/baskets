<?php

namespace App\Console\Tests;

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command baskets:clean-old success", function () {
    /** @var Basket $basketDelete */
    $basketDelete = Basket::factory()->create([
        'created_at' => now()->subHours(Setting::getValue(SettingCodeEnum::BASKET_DURATION))->subSecond(),
    ]);
    /** @var Basket $basketOk */
    $basketOk = Basket::factory()->create();
    BasketItem::factory()->for($basketOk)->count(3)->create();

    artisan("baskets:clean-old");

    assertDatabaseMissing((new Basket())->getTable(), [
        'id' => $basketDelete->id,
    ]);
    assertDatabaseHas((new BasketItem())->getTable(), [
        'basket_id' => $basketOk->id,
    ]);
});
