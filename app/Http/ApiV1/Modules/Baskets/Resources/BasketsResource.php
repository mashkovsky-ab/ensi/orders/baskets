<?php


namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Domain\Baskets\Models\Basket;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class BasketsResource
 * @package App\Http\ApiV1\Modules\Baskets\Resources
 * @mixin Basket
 */
class BasketsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'items' => BasketItemsResource::collection($this->whenLoaded('items')),
        ];
    }
}
