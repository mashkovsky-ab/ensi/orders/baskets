<?php


namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Domain\Baskets\Models\BasketItem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class BasketItemsResource
 * @package App\Http\ApiV1\Modules\Baskets\Resources
 * @mixin BasketItem
 */
class BasketItemsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'basket_id' => $this->basket_id,
            'offer_id' => $this->offer_id,
            'seller_id' => $this->seller_id,
            'product_id' => $this->product_id,
            'qty' => $this->qty,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }
}
