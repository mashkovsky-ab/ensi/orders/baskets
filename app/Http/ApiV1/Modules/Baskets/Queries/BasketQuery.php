<?php

namespace App\Http\ApiV1\Modules\Baskets\Queries;

use App\Domain\Baskets\Models\Basket;
use App\Http\ApiV1\Modules\Baskets\Requests\BasketCustomerFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BasketQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Basket::query();

        parent::__construct($query);

        $this->allowedIncludes(['items']);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
//            AllowedFilter::exact('id'),
        ]);

        $this->defaultSort('id');
    }

    public function customer(BasketCustomerFilter $request): self
    {
        $this->subject->where('customer_id', $request->getCustomerId());

        return $this;
    }
}
