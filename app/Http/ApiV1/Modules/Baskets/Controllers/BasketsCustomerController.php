<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Domain\Baskets\Actions\Basket\DeleteBasketAction;
use App\Domain\Baskets\Actions\Basket\PatchBasketSellerAction;
use App\Domain\Baskets\Actions\SetItems\SetItemsAction;
use App\Http\ApiV1\Modules\Baskets\Queries\BasketQuery;
use App\Http\ApiV1\Modules\Baskets\Requests\DeleteBasketCustomerRequest;
use App\Http\ApiV1\Modules\Baskets\Requests\PatchBasketCustomerSellerRequest;
use App\Http\ApiV1\Modules\Baskets\Requests\SearchBasketCustomerRequest;
use App\Http\ApiV1\Modules\Baskets\Requests\SetBasketCustomerItemRequest;
use App\Http\ApiV1\Modules\Baskets\Resources\BasketsResource;
use App\Http\ApiV1\Support\Resources\DataResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class BasketsCustomerController
{
    public function patchSeller(PatchBasketCustomerSellerRequest $request, PatchBasketSellerAction $action, BasketQuery $query)
    {
        $wasChange = $action->execute($query->customer($request)->firstOrFail(), $request->getSellerId());

        return new DataResource([
            'was_change' => $wasChange,
        ]);
    }

    public function setItem(SetBasketCustomerItemRequest $request, SetItemsAction $action)
    {
        $action->execute($request->convertToObject());

        return new EmptyResource();
    }

    public function searchOne(SearchBasketCustomerRequest $request, BasketQuery $query)
    {
        return new BasketsResource($query->customer($request)->firstOrFail());
    }

    public function delete(DeleteBasketCustomerRequest $request, DeleteBasketAction $action, BasketQuery $query)
    {
        $action->execute($query->customer($request)->firstOrFail());

        return new EmptyResource();
    }
}
