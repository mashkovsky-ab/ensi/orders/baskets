<?php


namespace App\Http\ApiV1\Modules\Baskets\Requests;

interface BasketCustomerFilter
{
    public function getCustomerId(): int;
}
