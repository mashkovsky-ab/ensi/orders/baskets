<?php

namespace App\Http\ApiV1\Modules\Baskets\Requests;

use App\Domain\Baskets\Actions\SetItems\Data\ItemData;
use App\Domain\Baskets\Actions\SetItems\Data\SetItemsData;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SetBasketCustomerItemRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'items' => ['required', 'array'],
            'items.*.offer_id' => ['required', 'integer'],
            'items.*.qty' => ['required', 'numeric'],
        ];
    }

    public function convertToObject(): SetItemsData
    {
        $data = new SetItemsData();
        $data->customerId = $this->get('customer_id');

        foreach ($this->get('items') as $item) {
            $itemData = new ItemData();
            $itemData->offerId = $item['offer_id'];
            $itemData->qty = $item['qty'];

            $data->addItem($itemData);
        }

        return $data;
    }
}
