<?php

namespace App\Http\ApiV1\Modules\Baskets\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SearchBasketCustomerRequest extends BaseFormRequest implements BasketCustomerFilter
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
        ];
    }

    public function getCustomerId(): int
    {
        return $this->get('customer_id');
    }
}
