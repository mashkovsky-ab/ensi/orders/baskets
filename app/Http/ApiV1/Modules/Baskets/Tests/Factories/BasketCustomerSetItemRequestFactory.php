<?php

namespace App\Http\ApiV1\Modules\Baskets\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class BasketCustomerSetItemRequestFactory extends BaseApiFactory
{
    protected array $items = [];

    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->randomNumber(),
            'items' => $this->items ?: [
                'qty' => $this->faker->randomFloat(4),
                'offer_id' => $this->faker->randomNumber(),
            ],
        ];
    }

    public function withItem(float $qty, int $offerId): static
    {
        $this->items[] = ['qty' => $qty, 'offer_id' => $offerId];

        return $this;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
