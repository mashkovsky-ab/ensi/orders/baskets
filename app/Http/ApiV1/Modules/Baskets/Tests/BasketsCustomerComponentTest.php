<?php

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Domain\Common\Tests\Factories\Catalog\OfferFactory;
use App\Domain\Common\Tests\Factories\Catalog\ProductFactory;
use App\Domain\Common\Tests\Factories\Catalog\StockFactory;
use App\Http\ApiV1\Modules\Baskets\Tests\Factories\BasketCustomerSetItemRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test("POST /api/v1/baskets/baskets/customer:search-one success", function () {
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $basketItems = BasketItem::factory()->for($basket)->count(3)->create();

    postJson("/api/v1/baskets/baskets/customer:search-one", [
        "customer_id" => $basket->customer_id,
        "include" => ["items"],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $basket->id)
        ->assertJsonCount($basketItems->count(), 'data.items');
});

test("POST /api/v1/baskets/baskets/customer:search-one 404", function () {
    postJson("/api/v1/baskets/baskets/customer:search-one", ["customer_id" => 2])
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/baskets/baskets/customer:set-item success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearchOne(['id' => $productId]),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withStock(StockFactory::new()->make(['qty' => $qty + 1]))->makeResponseSearch([
            'id' => $offerId,
            'product_id' => $productId,
        ]),
    ]);

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->make(["customer_id" => $basket->customer_id,]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseHas((new BasketItem())->getTable(), [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
        "qty" => $qty,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item many success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offer1Id = 1;
    $product1Id = 2;
    $offer2Id = 2;
    $product2Id = 2;
    $qty1 = 5.4;
    $qty2 = 10;

    $product1 = ProductFactory::new()->make(['id' => $product1Id]);
    $product2 = ProductFactory::new()->make(['id' => $product2Id]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearchMany([$product1, $product2]),
    ]);
    $offer1 = OfferFactory::new()
        ->withStock(StockFactory::new()->make(['qty' => $qty1 + 1]))
        ->make(['id' => $offer1Id, 'product_id' => $product1Id,]);
    $offer2 = OfferFactory::new()
        ->withStock(StockFactory::new()->make(['qty' => $qty2 + 1]))
        ->make(['id' => $offer2Id, 'product_id' => $product2Id,]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->makeResponseSearchFromItems([$offer1, $offer2]),
    ]);

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty1, $offer1Id)
        ->withItem($qty2, $offer2Id)
        ->make(["customer_id" => $basket->customer_id,]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseHas((new BasketItem())->getTable(), [
        "basket_id" => $basket->id,
        "offer_id" => $offer1Id,
        "qty" => $qty1,
    ]);
    assertDatabaseHas((new BasketItem())->getTable(), [
        "basket_id" => $basket->id,
        "offer_id" => $offer2Id,
        "qty" => $qty2,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item without basket success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $customerId = 3;
    $offerId = 1;
    $productId = 1;
    $qty = 10;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearchOne(['id' => $productId]),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withStock(StockFactory::new()->make(['qty' => $qty + 1]))->makeResponseSearch([
            'id' => $offerId,
            'product_id' => $productId,
        ]),
    ]);

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->make(["customer_id" => $customerId]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    /** @var Basket $basket */
    $basket = Basket::query()->where('customer_id', $customerId)->first();
    $this->assertNotNull($basket);

    assertDatabaseHas((new BasketItem())->getTable(), [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
        "qty" => $qty,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item inactive", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseEmpty(),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withStock(StockFactory::new()->make(['qty' => $qty + 1]))->makeResponseSearch([
            'id' => $offerId,
            'product_id' => $productId,
        ]),
    ]);

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->make(["customer_id" => $basket->customer_id,]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidateException");
});

# todo: check stock
//test("POST /api/v1/baskets/baskets/customer:set-item less stock", function () {
//    /** @var ApiV1ComponentTestCase $this */
//    /** @var Basket $basket */
//    $basket = Basket::factory()->create();
//    $offerId = 1;
//    $productId = 1;
//    $qty = 5.4;
//
//    $this->mockPimProductsApi()->allows([
//        'searchProducts' => ProductFactory::new()->makeResponseSearchOne(['id' => $productId]),
//    ]);
//    $this->mockOffersOffersApi()->allows([
//        'searchOffers' => OfferFactory::new()->withStock(StockFactory::new()->make(['qty' => $qty - 1]))->makeResponseSearch([
//            'id' => $offerId,
//            'product_id' => $productId,
//        ]),
//    ]);
//
//    $request = BasketCustomerSetItemRequestFactory::new()
//        ->withItem($qty, $offerId)
//        ->make(["customer_id" => $basket->customer_id,]);
//    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
//        ->assertStatus(400)
//        ->assertJsonPath('data', null)
//        ->assertJsonPath('errors.0.code', "ValidateException");
//});

test("POST /api/v1/baskets/baskets/customer:set-item delete success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete */
    $basketItemDelete = BasketItem::factory()->for($basket)->create();
    /** @var BasketItem $basketItemOk */
    $basketItemOk = BasketItem::factory()->for($basket)->create();

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem(0, $basketItemDelete->offer_id)
        ->make(["customer_id" => $basket->customer_id,]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseHas((new BasketItem())->getTable(), [
        'id' => $basketItemOk->id,
    ]);
    assertDatabaseMissing((new BasketItem())->getTable(), [
        'id' => $basketItemDelete->id,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item delete many success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete1 */
    $basketItemDelete1 = BasketItem::factory()->for($basket)->create();
    /** @var BasketItem $basketItemDelete2 */
    $basketItemDelete2 = BasketItem::factory()->for($basket)->create();

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem(0, $basketItemDelete1->offer_id)
        ->withItem(0, $basketItemDelete2->offer_id)
        ->make(["customer_id" => $basket->customer_id,]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseMissing((new BasketItem())->getTable(), [
        'basket_id' => $basket->id,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item add and delete success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $basketItemDelete */
    $basketItemDelete = BasketItem::factory()->for($basket)->create();
    $offerId = 1;
    $productId = 1;
    $qty = 5.4;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearchOne(['id' => $productId]),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withStock(StockFactory::new()->make(['qty' => $qty + 1]))->makeResponseSearch([
            'id' => $offerId,
            'product_id' => $productId,
        ]),
    ]);

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem($qty, $offerId)
        ->withItem(0, $basketItemDelete->offer_id)
        ->make(["customer_id" => $basket->customer_id,]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseHas((new BasketItem())->getTable(), [
        "basket_id" => $basket->id,
        "offer_id" => $offerId,
        "qty" => $qty,
    ]);
    assertDatabaseMissing((new BasketItem())->getTable(), [
        'id' => $basketItemDelete->id,
    ]);
});

test("POST /api/v1/baskets/baskets/customer:set-item delete undefined success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();

    $request = BasketCustomerSetItemRequestFactory::new()
        ->withItem(0, 1)
        ->make(["customer_id" => $basket->customer_id,]);
    postJson("/api/v1/baskets/baskets/customer:set-item", $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("DELETE /api/v1/baskets/baskets/customer:delete success", function () {
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->count(3)->create();

    deleteJson("/api/v1/baskets/baskets/customer:delete", [
        "customer_id" => $basket->customer_id,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertDatabaseMissing((new BasketItem())->getTable(), [
        'basket_id' => $basket->id,
    ]);
});

test("DELETE /api/v1/baskets/baskets/customer:delete 404", function () {
    deleteJson("/api/v1/baskets/baskets/customer:delete", ["customer_id" => 2,])
        ->assertStatus(404)
        ->assertJsonPath('data', null);
});

test("PATCH /api/v1/baskets/baskets/customer:patch-seller success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    /** @var BasketItem $itemChange */
    $itemChange = BasketItem::factory()->for($basket)->create([
        "offer_id" => 1,
        "seller_id" => 1,
        "product_id" => 1,
    ]);
    /** @var BasketItem $itemGood */
    $itemGood = BasketItem::factory()->for($basket)->create([
        "offer_id" => 2,
        "seller_id" => 2,
        "product_id" => 2,
    ]);
    /** @var BasketItem $itemDelete */
    $itemDelete = BasketItem::factory()->for($basket)->create([
        "offer_id" => 4,
        "seller_id" => 1,
        "product_id" => 3,
    ]);
    $newSeller = 2;
    $offerIdForOther = 3;

    // Запрос на получение активных товаров
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearchOne(['id' => $itemChange->product_id]),
    ]);

    // Запрос на получение предложений для нового продавца
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withStock(StockFactory::new()->make(['qty' => $itemChange->qty + 1]))->makeResponseSearch([
            'id' => $offerIdForOther,
            'seller_id' => $newSeller,
            'product_id' => $itemChange->product_id,
        ]),
    ]);

    patchJson("/api/v1/baskets/baskets/customer:patch-seller", [
        "customer_id" => $basket->customer_id,
        "seller_id" => $newSeller,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data', ['was_change' => true]);

    assertDatabaseHas((new BasketItem())->getTable(), [
        'basket_id' => $basket->id,
        'offer_id' => $itemGood->offer_id,
    ]);
    assertDatabaseHas((new BasketItem())->getTable(), [
        'basket_id' => $basket->id,
        'offer_id' => $offerIdForOther,
    ]);
    assertDatabaseMissing((new BasketItem())->getTable(), [
        'basket_id' => $basket->id,
        'offer_id' => $itemDelete->offer_id,
    ]);
});

test("PATCH /api/v1/baskets/baskets/customer:patch-seller 404", function () {
    patchJson("/api/v1/baskets/baskets/customer:patch-seller", ["customer_id" => 2, "seller_id" => 1,])
        ->assertStatus(404)
        ->assertJsonPath('data', null);
});
