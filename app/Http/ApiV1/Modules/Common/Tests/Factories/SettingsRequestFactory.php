<?php

namespace App\Http\ApiV1\Modules\Common\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class SettingsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'settings' => [
                [
                    'id' => $this->withId(),
                    'name' => $this->faker->text(50),
                    'value' => $this->faker->numerify('##'),
                ],
            ],
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
