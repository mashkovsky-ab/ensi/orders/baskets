<?php

namespace App\Http\ApiV1\Support\Tests;

use Ensi\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\PimClient\Api\ProductsApi;
use Mockery\MockInterface;
use Tests\ComponentTestCase;

abstract class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }

    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }
}
