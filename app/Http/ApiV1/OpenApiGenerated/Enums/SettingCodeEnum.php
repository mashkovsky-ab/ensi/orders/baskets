<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

class SettingCodeEnum
{
    public const BASKET_DURATION = 'basketStorageTime';

    /**
     * @return string[]
     */
    public static function cases(): array
    {
        return [
            self::BASKET_DURATION,
        ];
    }
}
