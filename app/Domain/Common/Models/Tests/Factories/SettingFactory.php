<?php

namespace App\Domain\Common\Models\Tests\Factories;

use App\Domain\Common\Models\Setting;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domain\Common\Models\Setting>
 */
class SettingFactory extends Factory
{
    protected $model = Setting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'code' => $this->faker->unique()->text(50),
            'name' => $this->faker->text(50),
            'value' => $this->faker->numerify('##'),
        ];
    }
}
