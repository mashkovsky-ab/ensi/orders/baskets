<?php

use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Constraint\IsType;
use Tests\IntegrationTestCase;
use function PHPUnit\Framework\assertNotNull;

uses(IntegrationTestCase::class);
uses()->group('integration');

// region SettingsPresence
test("Data SettingsPresence success", function (string $name, string $type) {
    /** @var Setting $setting */
    $setting = Setting::query()->where('code', $name)->first();

    assertNotNull($setting);
    Assert::assertThat($setting->value, new IsType($type));
})->with([
    'basketStorageTime' => [SettingCodeEnum::BASKET_DURATION, 'numeric'],
]);
// endregion
