<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OffersClient\Dto\Stock;

class StockFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'qty' => $this->faker->randomFloat(4),
        ];
    }

    public function make(array $extra = []): Stock
    {
        return new Stock($this->makeArray($extra));
    }
}
