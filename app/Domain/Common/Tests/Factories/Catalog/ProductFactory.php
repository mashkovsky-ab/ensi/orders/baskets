<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductStatusEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\SearchProductsResponse;
use Ensi\TestFactories\FactoryMissingValue;
use Illuminate\Support\Collection;

class ProductFactory extends BaseApiFactory
{
    public ?Collection $images = null;
    public ?Collection $attributes = null;

    protected function definition(): array
    {
        $definition = [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug,
            'description' => $this->faker->text,
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'status_id' => $this->faker->randomElement(ProductStatusEnum::getAllowableEnumValues()),
            'status_comment' => $this->faker->optional->sentence,
            'allow_publish' => $this->faker->boolean,

            'external_id' => $this->faker->unique()->numerify('######'),
            'barcode' => $this->faker->ean13,
            'vendor_code' => $this->faker->numerify('###-###-###'),

            'weight' => $this->faker->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->randomFloat(3, 1, 100),
            'length' => $this->faker->randomFloat(2, 1, 1000),
            'height' => $this->faker->randomFloat(2, 1, 1000),
            'width' => $this->faker->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,

            'category_id' => $this->faker->randomNumber(),
            'images' => $this->executeNested($this->images, new FactoryMissingValue()),
            'attributes' => $this->whenNotNull($this->attributes, $this->attributes?->all()),
        ];

        return $definition;
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponseSearchOne(array $extra = []): SearchProductsResponse
    {
        return new SearchProductsResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }

    public function makeResponseSearchMany(array $items): SearchProductsResponse
    {
        return new SearchProductsResponse(['data' => $items]);
    }

    public function makeResponseEmpty(): SearchProductsResponse
    {
        return new SearchProductsResponse([
            'data' => [],
        ]);
    }
}
