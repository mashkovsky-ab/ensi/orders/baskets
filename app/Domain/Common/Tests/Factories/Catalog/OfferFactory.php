<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferResponse;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\OffersClient\Dto\Stock;

class OfferFactory extends BaseApiFactory
{
    protected array $stocks = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
            'product_id' => $this->faker->randomNumber(),
        ];

        if ($this->stocks) {
            $definition['stocks'] = $this->stocks;
        }

        return $definition;
    }

    public function withStock(?Stock $stock): self
    {
        $this->stocks[] = $stock ?: StockFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): OfferResponse
    {
        return new OfferResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extra, $count);
    }

    public function makeResponseSearchFromItems(array $items): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, [], 1, $items);
    }
}
