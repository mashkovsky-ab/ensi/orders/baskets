<?php

namespace App\Domain\Baskets\Actions\SetItems\Data;

class SetItemsData
{
    public int $customerId;
    /** @var ItemData[] */
    protected array $addItems = [];
    /** @var ItemData[] */
    protected array $deleteItems = [];

    public function addItem(ItemData $item)
    {
        if ($item->qty > 0) {
            $this->addItems[] = $item;
        } else {
            $this->deleteItems[] = $item;
        }
    }

    public function addItems(): array
    {
        return $this->addItems;
    }

    public function deleteItems(): array
    {
        return $this->deleteItems;
    }
}
