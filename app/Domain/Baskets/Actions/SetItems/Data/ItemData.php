<?php

namespace App\Domain\Baskets\Actions\SetItems\Data;

class ItemData
{
    public int $offerId;
    public float $qty;
}
