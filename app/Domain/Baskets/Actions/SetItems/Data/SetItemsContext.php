<?php

namespace App\Domain\Baskets\Actions\SetItems\Data;

use App\Domain\Baskets\Models\Basket;
use Illuminate\Database\Eloquent\Collection;

class SetItemsContext
{
    public ?Basket $basket;
    /** @var ProductInfoData[] */
    protected array $products = [];

    public function __construct(public SetItemsData $data)
    {
        $this->loadBasket();
    }

    protected function loadBasket()
    {
        $this->basket = Basket::query()
            ->where('customer_id', $this->data->customerId)
            ->with('items')
            ->first();
    }

    public function getBasketOrCreate(): Basket
    {
        if (!$this->basket) {
            $this->basket = new Basket();
            $this->basket->customer_id = $this->data->customerId;
            $this->basket->save();
            $this->basket->setRelation('items', new Collection());
        }

        return $this->basket;
    }

    public function getProductInfo(int $offerId): ProductInfoData
    {
        return $this->products[$offerId];
    }

    public function setProductInfo(int $offerId, ProductInfoData $data)
    {
        $this->products[$offerId] = $data;
    }
}
