<?php

namespace App\Domain\Baskets\Actions\SetItems\Data;

use Ensi\OffersClient\Dto\Offer;

class ProductInfoData
{
    public Offer $offer;
}
