<?php

namespace App\Domain\Baskets\Actions\SetItems;

use App\Domain\Baskets\Actions\SetItems\Data\SetItemsContext;
use App\Domain\Baskets\Actions\SetItems\Data\SetItemsData;
use App\Domain\Baskets\Actions\SetItems\Stages\AddItemsAction;
use App\Domain\Baskets\Actions\SetItems\Stages\DeleteItemsAction;
use Illuminate\Support\Facades\DB;

class SetItemsAction
{
    protected SetItemsContext $context;

    public function __construct(
        protected AddItemsAction $addItemsAction,
        protected DeleteItemsAction $deleteItemsAction,
    ) {
    }

    public function execute(SetItemsData $data)
    {
        $this->context = new SetItemsContext($data);

        DB::transaction(function () {
            $this->addItemsAction->execute($this->context);
            $this->deleteItemsAction->execute($this->context);
        });
    }
}
