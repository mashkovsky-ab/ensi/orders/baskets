<?php

namespace App\Domain\Baskets\Actions\SetItems\Stages;

use App\Domain\Baskets\Actions\SetItems\Data\SetItemsContext;
use App\Domain\Baskets\Models\BasketItem;

class DeleteItemsAction
{
    public function execute(SetItemsContext $context)
    {
        if (!$context->basket || !$context->data->deleteItems()) {
            return;
        }

        $basketItems = $context->basket->items->keyBy('offer_id');
        foreach ($context->data->deleteItems() as $itemData) {
            /** @var BasketItem $item */
            $item = $basketItems->get($itemData->offerId);

            if (!$item) {
                continue;
            }

            $item->delete();
        }
    }
}
