<?php

namespace App\Domain\Baskets\Actions\SetItems\Stages;

use App\Domain\Baskets\Actions\SetItems\Data\SetItemsContext;
use App\Domain\Baskets\Models\BasketItem;

class AddItemsAction
{
    public function __construct(
        protected LoadProductInfoAction $loadProductInfoAction,
    ) {
    }

    public function execute(SetItemsContext $context)
    {
        if (!$context->data->addItems()) {
            return;
        }
        $this->loadProductInfoAction->execute($context);

        $basket = $context->getBasketOrCreate();
        $basketItems = $basket->items->keyBy('offer_id');
        foreach ($context->data->addItems() as $itemData) {
            /** @var BasketItem|null $item */
            $item = $basketItems->get($itemData->offerId);
            $productData = $context->getProductInfo($itemData->offerId);
            if (!$item) {
                $item = new BasketItem();
                $item->offer_id = $itemData->offerId;
                $item->basket_id = $basket->id;
                $item->seller_id = $productData->offer->getSellerId();
                $item->product_id = $productData->offer->getProductId();

                $item->setRelation('basket', $basket);
                $item->makeHidden('basket');
            }

            $item->qty = $itemData->qty;

            $item->save();
        }
    }
}
