<?php


namespace App\Domain\Baskets\Actions\SetItems\Stages;

use App\Domain\Baskets\Actions\SetItems\Data\ProductInfoData;
use App\Domain\Baskets\Actions\SetItems\Data\SetItemsContext;
use App\Exceptions\ValidateException;
use Arr;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OffersClient\Dto\Stock;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Collection;

class LoadProductInfoAction
{
    public function __construct(
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
    ) {
    }

    public function execute(SetItemsContext $context): void
    {
        $offerIds = Arr::pluck($context->data->addItems(), 'offerId');
        $offers = $this->loadOffers($offerIds);
        $productIds = $offers->pluck('product_id')->values()->toArray();

        if (empty($productIds) || !$this->checkProductsActive($productIds)) {
            throw new ValidateException('В запросе присутствует неактивный товар');
        }

        foreach ($context->data->addItems() as $item) {
            $offer = $offers[$item->offerId] ?? null;
            if (!$offer) {
                throw new ValidateException("Для товара {$item->offerId} не найден оффер");
            }

            # todo: check stock
//            /** @var Stock $stock */
//            $stock = collect($offer->getStocks())->first();
//            if (!$stock) {
//                throw new ValidateException("Для товара {$item->offerId} не найден сток");
//            }
//            if ($stock->getQty() < $item->qty) {
//                throw new ValidateException("Кол-ва товара {$item->offerId} недостаточно на складе");
//            }

            $productInfo = new ProductInfoData();
            $productInfo->offer = $offer;

            $context->setProductInfo($item->offerId, $productInfo);
        }
    }

    protected function checkProductsActive(array $productIds): bool
    {
        $request = new SearchProductsRequest();
        $request->setFilter((object)[
            'allow_publish' => true,
            'id' => $productIds,
        ]);

        $searchResult = $this->productsApi->searchProducts($request);

        return count($searchResult->getData()) == count($productIds);
    }

    /**
     * @param int[] $offerIds
     * @return Collection|Offer[]
     */
    protected function loadOffers(array $offerIds): Collection
    {
        $searchOffersRequest = new SearchOffersRequest();
        $searchOffersRequest->setFilter((object)[
            'id' => $offerIds,
        ]);
        $searchOffersRequest->setInclude(['stocks']);

        return collect($this->offersApi->searchOffers($searchOffersRequest)->getData())->keyBy('id');
    }
}
