<?php

namespace App\Domain\Baskets\Actions\Basket;

use App\Domain\Baskets\Models\Basket;
use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Illuminate\Database\Eloquent\Collection;

class CleanOldBasketsAction
{
    public function __construct(
        protected DeleteBasketAction $deleteBasketAction,
    ) {
    }

    public function execute()
    {
        Basket::query()
            ->where('created_at', '<', now()->subHours(Setting::getValue(SettingCodeEnum::BASKET_DURATION)))
            ->orderBy('created_at')
            ->chunkById(500, function ($baskets) {
                /** @var Collection|Basket[] $baskets */
                foreach ($baskets as $basket) {
                    $this->deleteBasketAction->execute($basket);
                }
            });
    }
}
