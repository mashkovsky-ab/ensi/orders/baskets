<?php

namespace App\Domain\Baskets\Actions\Basket;

use App\Domain\Baskets\Actions\SetItems\Data\ItemData;
use App\Domain\Baskets\Actions\SetItems\Data\SetItemsData;
use App\Domain\Baskets\Actions\SetItems\SetItemsAction;
use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Exceptions\ValidateException;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PatchBasketSellerAction
{
    public function __construct(
        protected OffersApi $offersApi,
        protected SetItemsAction $setBasketItemAction,
    ) {
    }

    public function execute(?Basket $basket, int $newSellerId): bool
    {
        $wasChange = false;
        if (!$basket) {
            return $wasChange;
        }

        $itemsForChange = $basket->items->filter(function (BasketItem $basketItem) use ($newSellerId) {
            return $basketItem->seller_id != $newSellerId;
        });

        if ($itemsForChange->isEmpty()) {
            return $wasChange;
        }

        // Массив соответствия старых офферов товарам [oldOfferId => productId]
        $productIdByOfferId = $itemsForChange->pluck('product_id', 'offer_id')->all();

        // Массив соответствия товаров новым офферам [productId => offer_id]
        $offerInNewSeller = empty($productIdByOfferId)
            ? new Collection()
            : $this->loadOffers(array_values($productIdByOfferId), $newSellerId);

        // Теперь удаляем старые товары из корзины, по возможности заменяя на новые
        DB::transaction(function () use ($itemsForChange, $basket, $productIdByOfferId, $offerInNewSeller, &$wasChange) {
            $setItemsData = new SetItemsData();
            $setItemsData->customerId = $basket->customer_id;

            foreach ($itemsForChange as $oldBasketItem) {
                $productId = $productIdByOfferId[$oldBasketItem->offer_id] ?? null;

                if ($productId) {
                    $newOfferId = $offerInNewSeller->get($productId);

                    if ($newOfferId) {
                        // Если новый оффер, соответствующий старому, нашелся, то пытаемся его добавить в корзину
                        $setItem = new ItemData();
                        $setItem->offerId = $newOfferId;
                        $setItem->qty = $oldBasketItem->qty;
                        $setItemsData->addItem($setItem);
                    }
                }

                $oldBasketItem->delete();
                $wasChange = true;
            }

            try {
                $this->setBasketItemAction->execute($setItemsData);
            } catch (ValidateException $e) {
            }
        });

        return $wasChange;
    }

    /**
     * @param int[] $productIds
     * @return Collection<int,Offer>
     */
    private function loadOffers(array $productIds, int $sellerId): Collection
    {
        $request = new SearchOffersRequest();
        $request->setFilter((object)[
            'product_id' => $productIds,
            'seller_id' => $sellerId,
            'sale_status' => OfferSaleStatusEnum::ON_SALE,
        ]);

        return collect($this->offersApi->searchOffers($request)->getData())
            ->mapWithKeys(fn (Offer $offer) => [$offer->getProductId() => $offer->getId()]);
    }
}
