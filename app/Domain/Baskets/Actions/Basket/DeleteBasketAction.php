<?php

namespace App\Domain\Baskets\Actions\Basket;

use App\Domain\Baskets\Models\Basket;

class DeleteBasketAction
{
    public function execute(Basket $basket)
    {
        foreach ($basket->items as $item) {
            $item->delete();
        }
        $basket->delete();
    }
}
