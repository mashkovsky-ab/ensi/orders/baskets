<?php

namespace App\Domain\Baskets\Models;

use App\Domain\Baskets\Models\Tests\Factories\BasketItemFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $basket_id id корзины
 * @property int $offer_id id предложения продавца
 * @property int $seller_id id продавца
 * @property int $product_id id товара
 * @property float $qty кол-во товара
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Basket $basket
 */
class BasketItem extends Model
{
    protected $table = 'basket_items';

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'qty' => 'float',
    ];

    public static function factory(): BasketItemFactory
    {
        return BasketItemFactory::new();
    }

    public function basket(): BelongsTo
    {
        return $this->belongsTo(Basket::class);
    }
}
