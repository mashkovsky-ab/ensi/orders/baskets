<?php

namespace App\Domain\Baskets\Models;

use App\Domain\Baskets\Models\Tests\Factories\BasketFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property int $customer_id id покупателя
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Collection|BasketItem[] $items - элементы
 */
class Basket extends Model
{
    protected $table = 'baskets';

    public static function factory(): BasketFactory
    {
        return BasketFactory::new();
    }

    public function items(): HasMany
    {
        return $this->hasMany(BasketItem::class);
    }
}