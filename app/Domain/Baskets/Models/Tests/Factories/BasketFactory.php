<?php

namespace App\Domain\Baskets\Models\Tests\Factories;

use App\Domain\Baskets\Models\Basket;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domain\Baskets\Models\Basket>
 */
class BasketFactory extends Factory
{
    protected $model = Basket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->unique()->randomNumber(),
        ];
    }
}
