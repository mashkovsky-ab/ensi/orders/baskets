<?php

namespace App\Domain\Baskets\Models\Tests\Factories;

use App\Domain\Baskets\Models\BasketItem;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domain\Baskets\Models\BasketItem>
 */
class BasketItemFactory extends Factory
{
    protected $model = BasketItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @return array<string, mixed>
     */
    public function definition()
    {
        $basketId = $this->faker->randomNumber();
        $offerCode = $this->faker->unique()->regexify("/^$basketId-[0-9]{5}"); // Уникальный оффер id для корзины
        $offerId = explode('-', $offerCode)[1];
        $offerInfo = $this->faker->unique()->regexify("/^$offerId-[0-9]{5}-[0-9]{5}"); // Уникальные товар и продавец, соответствующие офферу
        $productId = explode('-', $offerInfo)[1];
        $sellerId = explode('-', $offerInfo)[2];

        return [
            'basket_id' => $this->faker->randomNumber(),
            'offer_id' => (int)$offerId,
            'seller_id' => (int)$sellerId,
            'product_id' => (int)$productId,
            'qty' => $this->faker->randomFloat(2, 0, 100),
        ];
    }
}
