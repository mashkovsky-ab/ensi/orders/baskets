<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Baskets\Actions\Basket\DeleteBasketAction;
use App\Domain\Baskets\Models\Basket;
use App\Domain\Kafka\Messages\Listen\Order\OrderEventMessage;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderItem;
use Illuminate\Database\Eloquent\Collection;
use RdKafka\Message;

class ListenOrderAction
{
    public function __construct(
        protected OrdersApi $ordersApi,
        protected DeleteBasketAction $deleteBasketAction
    ) {
    }

    public function execute(Message $message)
    {
        $eventMessage = OrderEventMessage::makeFromRdKafka($message);
        if ($eventMessage->event != OrderEventMessage::CREATE) {
            return;
        }

        /** @var Basket|null $basket */
        $basket = Basket::query()->where('customer_id', $eventMessage->attributes->customer_id)->first();
        if (!$basket) {
            return;
        }

        $order = $this->ordersApi->getOrder($eventMessage->attributes->id, 'items')->getData();
        $deleteItems = collect($order->getItems())->keyBy('offer_id');
        $basket->loadMissing('items');

        $issetItems = false;
        foreach ($basket->items as $item) {
            /** @var OrderItem|null $deleteItem */
            $deleteItem = $deleteItems->get($item->offer_id);
            if ($deleteItem) {
                if ($deleteItem->getQty() >= $item->qty) {
                    $item->delete();

                    continue;
                } else {
                    $item->qty -= $deleteItem->getQty();
                    $item->save();
                }
            }

            $issetItems = true;
        }

        if (!$issetItems) {
            $basket->setRelation('items', new Collection());
            $this->deleteBasketAction->execute($basket);
        }
    }
}
