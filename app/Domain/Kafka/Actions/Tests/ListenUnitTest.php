<?php

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Domain\Common\Tests\Factories\Orders\OrderFactory;
use App\Domain\Common\Tests\Factories\Orders\OrderItemFactory;
use App\Domain\Kafka\Actions\Listen\ListenOrderAction;
use App\Domain\Kafka\Messages\Listen\Order\OrderEventMessage;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\OrderEventMessageFactory;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('unit');

test("Action ListenOrderAction success", function (array $items) {
    /** @var ComponentTestCase $this */
    $customerId = 1;
    $orderId = 1;
    /** @var Basket $basket */
    $basket = Basket::factory()->create(['customer_id' => $customerId]);
    $orderFactory = OrderFactory::new();
    $missingItemsData = [];
    $existItemsData = [];
    foreach ($items as $k => $item) {
        $offerId = $k + 1;

        if ($item['basket_qty']) {
            /** @var BasketItem $basketItem */
            $basketItem = BasketItem::factory()->for($basket)->create([
                'offer_id' => $offerId,
                'qty' => $item['basket_qty'],
            ]);
            if (!$item['exist']) {
                $missingItemsData[] = ['id' => $basketItem->id];
            } else {
                $existItemsData[] = ['id' => $basketItem->id, 'qty' => $item['basket_qty'] - $item['order_qty']];
            }
        }

        if ($item['order_qty']) {
            $orderFactory->withItem(OrderItemFactory::new()->make([
                'offer_id' => $offerId,
                'qty' => $item['order_qty'],
            ]));
        }
    }

    $this->mockOmsOrdersApi()->allows([
        'getOrder' => $orderFactory->makeResponseOne(),
    ]);

    $message = OrderEventMessageFactory::new()->setEvent(OrderEventMessage::CREATE)->make(['attributes' => [
        'id' => $orderId,
        'customer_id' => $customerId,
    ]]);
    resolve(ListenOrderAction::class)->execute($message);

    if (!$existItemsData) {
        assertDatabaseMissing((new Basket())->getTable(), [
            'id' => $basket->id,
        ]);
    }
    foreach ($missingItemsData as $missingItem) {
        assertDatabaseMissing((new BasketItem())->getTable(), $missingItem);
    }
    foreach ($existItemsData as $existItem) {
        assertDatabaseHas((new BasketItem())->getTable(), $existItem);
    }
})->with([
    'order full' => [[['basket_qty' => 1.2, 'order_qty' => 1.2, 'exist' => false]]],
    'order part' => [[['basket_qty' => 1.2, 'order_qty' => 1, 'exist' => true]]],
    'order more' => [[['basket_qty' => 0, 'order_qty' => 1], ['basket_qty' => 1, 'order_qty' => 1, 'exist' => false]]],
    'basket more' => [[['basket_qty' => 1, 'order_qty' => 0, 'exist' => true], ['basket_qty' => 1, 'order_qty' => 1, 'exist' => false]]],
]);
