<?php

namespace App\Domain\Kafka\Messages\Listen\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\Order\OrderEventMessage;
use Ensi\TestFactories\Factory;
use RdKafka\Message;

class OrderEventMessageFactory extends Factory
{
    protected ?string $event = null;

    protected function definition(): array
    {
        $event = $this->event ?: $this->faker->randomElement([
            OrderEventMessage::CREATE,
            OrderEventMessage::UPDATE,
            OrderEventMessage::DELETE,
        ]);

        return [
            'event' => $event,
            'attributes' => [
                'id' => $this->faker->randomNumber(),
                'customer_id' => $this->faker->randomNumber(),
            ],
            'dirty' => $event == OrderEventMessage::UPDATE ? $this->faker->randomElements([
                'status',
            ]) : null,
        ];
    }

    public function setEvent(string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }

    protected function mergeDefinitionWithExtra(array $extra): array
    {
        $extraAttributes = $extra['attributes'] ?? [];
        unset($extra['attributes']);
        $array = parent::mergeDefinitionWithExtra($extra);

        $array['attributes'] = array_merge($array['attributes'], $extraAttributes);

        return $array;
    }
}
