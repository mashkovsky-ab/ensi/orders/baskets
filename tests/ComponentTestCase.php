<?php

namespace Tests;

use Ensi\OmsClient\Api\OrdersApi;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery\MockInterface;

class ComponentTestCase extends TestCase
{
    use DatabaseTransactions;

    protected function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }
}
